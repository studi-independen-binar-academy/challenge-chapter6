const express = require("express");
const controllers = require("../app/controllers");
const carsApi = require("../app/controllers/api/v1/cars")

const appRouter = express.Router();
const apiRouter = express.Router();

// swagger configuration and initialization
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

apiRouter.use('/api-docs', swaggerUi.serve);
apiRouter.get('/api-docs', swaggerUi.setup(swaggerDocument));

/** Mount GET / handler */
// appRouter.get("/", controllers.main.index);

/**
 * TODO: Implement your own API
 *       implementations
 */
// Register Members
apiRouter.post("/api/v1/users/register", controllers.api.v1.users.register);

// Login as superadmin, admin, and members
apiRouter.post("/api/v1/users/login", controllers.api.v1.users.login);

// Adding admin by superadmin only
apiRouter.post(
  "/api/v1/users/addadmin",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isSuperAdmin,
  controllers.api.v1.users.addAdmin
);

// Update by superadmin only
apiRouter.put(
  "/api/v1/users/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isSuperAdmin,
  controllers.api.v1.users.update
);

// Authorize Users
apiRouter.get(
  "/api/v1/users/whoami",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.whoAmI
);

// Get All Users
apiRouter.get("/api/v1/users", controllers.api.v1.users.getUsers);

// Create Cars
apiRouter.post(
  "/api/v1/cars",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  carsApi.create
);

// Update Cars
apiRouter.put(
  "/api/v1/cars/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  carsApi.update
);

// Show Cars By ID
apiRouter.get(
  "/api/v1/cars/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  carsApi.show
);

// Fill in deletedBy
apiRouter.put(
  "/api/v1/cars/delete/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  carsApi.delete
)

// Show All Cars List
apiRouter.get("/api/v1/cars", carsApi.list);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
