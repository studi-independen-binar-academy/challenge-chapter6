'use strict';

const bcrypt = require("bcryptjs");
const SALT = 10;

function encryptPassword(userPassword) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(userPassword, SALT, (err, password) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(password);
    });
  });
}

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Users', [{
      name: 'tubagus',
      email: 'tubagus@mail.com',
      password: await encryptPassword('test'),
      role: 'superadmin',
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
