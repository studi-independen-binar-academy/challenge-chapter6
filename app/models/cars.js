'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Cars.init({
    name: DataTypes.STRING,
    type: DataTypes.STRING,
    size: DataTypes.STRING,
    price: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    deletedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Cars',
  });

  Cars.associate = function (models) {
    Cars.belongsTo(models.Users, {
      foreignKey: 'id',
      as: 'usersId'
    })
  }

  return Cars;
};