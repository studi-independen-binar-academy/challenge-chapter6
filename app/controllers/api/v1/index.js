/**
 * @file contains entry point of controllers api v1 module
 * @author Tubagus Mochammad Eza
 */

 const users = require("./users");

module.exports = {
  users
};
