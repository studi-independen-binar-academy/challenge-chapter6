/**
 * @file contains entry point of controllers api module
 * @author Tubagus Mochammad Eza
 */

const main = require("./main");
const v1 = require("./v1");

module.exports = {
  main,
  v1,
};
