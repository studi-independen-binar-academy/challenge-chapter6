/**
 * @file contains entry point of controllers module
 * @author Tubagus Mochammad Eza
 */

const api = require("./api");

module.exports = {
  api,
};
