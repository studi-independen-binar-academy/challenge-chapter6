/**
 * @file Bootstrap express.js server
 * @author Tubagus Mochammad Eza
 */

const express = require("express");
const morgan = require("morgan");
const router = require("../config/routes");
const cors = require("cors");

const app = express();

/** Install request logger */
app.use(morgan("dev"));

/** Install JSON request parser */
app.use(express.json());

/** Install Router */
app.use(router);

/** Install cors */
app.use(cors());
module.exports = app;
